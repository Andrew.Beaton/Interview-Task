
import pandas as pd 
import os

def load_master():
    """
    Controls the loading of 
    Input: 
    Output: pandas object excel file 
    """

    data_path =  create_path()
    file_object  = load_xlsx(data_path)
    return file_object
    
def load_xlsx(path:str):
    """
    Function loads and returns xlsx files.
    Input: String 
    Output : xlsx Class object
    """
    
    return pd.ExcelFile(path)


def create_path():
    """
    Creates path to data 
    Input:  
    Output : string
    """
    current_dir  =  os.path.realpath('')

    data_path = os.path.join(current_dir,"qof-1920-prev-ach-pca-cv-prac.xlsx")
    
    return data_path