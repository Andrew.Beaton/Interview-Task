import pandas as pd 


def initial_cleaning(raw : object ):
    """
    Basic data cleaning before initial analysis 
    input: Pandas dataframe
    output: pandas dataframe 
    """
    col_names = name_cols(raw)
    
    r_na = remove_null_rows(col_names)
    r_na.reset_index(inplace = True)
    
    processed = remove_non_data(r_na)
    
    
    return processed
    
    
def name_cols(df :object):
    """
    sets columns names from data to the column title
    input: Pandas dataframe
    output: pandas dataframe 
    """
    df.columns = col_list() #df.iloc[12,:]
    return df 


def remove_null_rows(df:object ):
    """
    Removes any rows that are entirely null
    input: Pandas dataframe
    output: pandas dataframe 
    """
    df.dropna(axis = 0, inplace = True)
    return df

def remove_non_data(df:object):
    """
    Removes cells that are not part of the main data 
    input: Pandas dataframe
    output: pandas dataframe 
    """
    df.drop([0], inplace = True)
    return df 
    
def col_list():
    """
    Provides a custom 
    input: 
    output: list
    """
    cols = ["CCG ODS code",
    "CCG ONS code",
    "CCG name",	
    "PCN ODS code",	
    "PCN name",	
    "Practice code",	
    "Practice name",	
    "18-19 List size ages 30-74",
    "18-19 Register",
    "18-19 Prevalence",
    "19-20 List size ages 30-74	",
    "19-20 Register	",
    "19-20 Prevalence",
    "18-20 Year on year change Prevalence",	
    "18-19 Total Achievement Score",
    "18-19 Achievement",
    "19-20 Total Achievement Score",
    "19-20 Achievement",
    "18-20 Year on year change Achievement",	
    "18-19 Total Exceptions",	
    "18-19 Total Denominators",
    "18-19 Exception Rate",	
    "19-20 Total PCAs",	
    "19-20 Total Denominators",	
    "19-20 PCA Rate",
    "Year on year change perc PCA",	
    "Achievement Score CVD",
    "Numerator CVD",	
    "Denominator CVD",
    "Underlying Achievement net of PCAs CVD",
    "PCAs CVD",
    "PCA Rate CVD", 	
    "Denominator plus PCAs CVD", 	
    "patients receiving intervention CVD"]
    return cols