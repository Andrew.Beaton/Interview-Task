import pandas as pd 


def initial_cleaning(raw : object ):
    """
    Basic data cleaning before initial analysis 
    input: Pandas dataframe
    output: pandas dataframe 
    """
    col_names = name_cols(raw) 
    col_names.reset_index(inplace = True)
    num_conv =convert_cols(col_names)
    processed =num_conv
    
    #processed = remove_null_rows(col_names)
    return processed
    
    
def name_cols(df :object):
    """
    sets columns names from data to the column title
    input: Pandas dataframe
    output: pandas dataframe 
    """
    df.columns = col_list() #df.iloc[12,:]
    df = remove_non_data(df)
    return df 

def convert_cols(df : object):
    """
    Converts data type of numeric and string columns to numeric and string dtype
    input: Pandas dataframe
    output: pandas dataframe 
    """
    num_cols_list = numeric_cols()
    for col in num_cols_list :
        df[col] = pd.to_numeric(df[col],errors="coerce").dropna()
    
    string_cols_list = string_cols()
    
    for col in string_cols_list :
        df[col] = df[col].apply(str).dropna()
    
    return df


def remove_null_rows(df:object ):
    """
    Removes any rows that are entirely null
    input: Pandas dataframe
    output: pandas dataframe 
    """
    df.dropna(axis = 0, inplace = True)
    return df

def remove_non_data(df:object):
    """
    Removes cells that are not part of the main data 
    input: Pandas dataframe
    output: pandas dataframe 
    """
    df.drop([0], inplace = True)
    return df 
    
def col_list():
    """
    Provides a custom list of column titles 
    input: 
    output: list
    """
    cols = ["CCG ODS code",
    "CCG ONS code",
    "CCG name",
    "PCN ODS code",
    "PCN name",
    "Practice code",
    "Practice name",
    "18-19 List size ages 30-74",
    "18-19 Register",
    "18-19 Prevalence",
    "19-20 List size ages 30-74",
    "19-20 Register	",
    "19-20 Prevalence",
    "18-20 Year on year change Prevalence",
    "18-19 Total Achievement Score",
    "18-19 Achievement",
    "19-20 Total Achievement Score",
    "19-20 Achievement",
    "18-20 Year on year change Achievement",
    "18-19 Total Exceptions",
    "18-19 Total Denominators",
    "18-19 Exception Rate",
    "19-20 Total PCAs",
    "19-20 Total Denominators",
    "19-20 PCA Rate",
    "Year on year change perc PCA",
    "Achievement Score CVD",
    "Numerator CVD",
    "Denominator CVD",
    "Underlying Achievement net of PCAs CVD",
    "PCAs CVD",
    "PCA Rate CVD", 
    "Denominator plus PCAs CVD", 
    "patients receiving intervention CVD"]
    return cols

def numeric_cols():
    """
    Provides a custom list of column titles which are numeric
    input: 
    output: list
    """
    num_cols = ["18-19 List size ages 30-74",
    "18-19 Register",
    "18-19 Prevalence",
    "19-20 List size ages 30-74",
    "19-20 Register	",
    "19-20 Prevalence",
    "18-20 Year on year change Prevalence",
    "18-19 Total Achievement Score",
    "18-19 Achievement",
    "19-20 Total Achievement Score",
    "19-20 Achievement",
    "18-20 Year on year change Achievement",
    "18-19 Total Exceptions",
    "18-19 Total Denominators",
    "18-19 Exception Rate",
    "19-20 Total PCAs",
    "19-20 Total Denominators",
    "19-20 PCA Rate",
    "Year on year change perc PCA",
    "Achievement Score CVD",
    "Numerator CVD",
    "Denominator CVD",
    "Underlying Achievement net of PCAs CVD",
    "PCAs CVD",
    "PCA Rate CVD", 
    "Denominator plus PCAs CVD", 
    "patients receiving intervention CVD"]
    return num_cols

def string_cols():
    """
    Provides a custom list of column titles which are strings
    input: 
    output: list
    """
    string_cols = ["CCG ODS code",
    "CCG ONS code",
    "CCG name",
    "PCN ODS code",
    "PCN name",
    "Practice code",
    "Practice name",]
    return string_cols